#include <string>
using namespace std;

class Kitten
{
    public:
    Kitten();
    ~Kitten();

    void Setup( string name, int age );
    void Display();

    static int GetCount();

    private:
    int m_age;
    string m_name;

    static int s_counter;
};
