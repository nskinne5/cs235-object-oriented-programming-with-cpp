#include <iostream>
using namespace std;

#include "SmartDynamicArray.hpp"

int main()
{
    SmartDynamicArray myArray;

    try
    {
        string obj = myArray.Get( 100 );
    }
    catch( out_of_range& ex )
    {
        cout << "Error! " << ex.what() << endl;
        throw ex;
    }
    catch( runtime_error& ex )
    {
        cout << "Error! " << ex.what() << endl;
        throw ex;
    }

    return 0;
}
