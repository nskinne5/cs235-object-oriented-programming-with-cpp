#include "Employee.hpp"

#include <iostream>
using namespace std;

// default constructor
Employee::Employee()
{
    m_ptrSupervisor = nullptr;
}

// parameterized constructor
Employee::Employee( string name, string title, Employee* ptrManager )
{
    Setup( name, title, ptrManager );
}

// copy constructor
Employee::Employee( const Employee& other )
{
    m_jobTitle      = other.m_jobTitle;
    m_ptrSupervisor = other.m_ptrSupervisor;
}

void Employee::SetName( string name )
{
    m_fullName = name;
}

void Employee::Setup( string name, string title, Employee* ptrManager )
{
    m_fullName      = name;
    m_jobTitle      = title;
    m_ptrSupervisor = ptrManager;
}

void Employee::Display()
{
    cout << "Employee: .... " << m_fullName << endl;
    cout << "Title: ....... " << m_jobTitle << endl;
    cout << "Supervisor: .. ";
    if ( m_ptrSupervisor == nullptr )
    {
        cout << "None" << endl;
    }
    else
    {
        cout << m_ptrSupervisor->m_fullName << endl;
    }

    cout << endl;
}
