#ifndef _EMPLOYEE_HPP
#define _EMPLOYEE_HPP

#include <string>
using namespace std;

class Employee
{
    public:
    // default constructor
    Employee();

    // parameterized constructor
    Employee( string name, string title, Employee* ptrManager );

    // copy constructor
    Employee( const Employee& other );

    void SetName( string name );
    void Setup( string name, string title, Employee* ptrManager );
    void Display();

    private:
    string m_fullName;
    string m_jobTitle;
    Employee* m_ptrSupervisor;
};

#endif
