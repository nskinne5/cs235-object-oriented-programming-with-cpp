\contentsline {chapter}{\numberline {1}Turn-in instructions}{2}
\contentsline {chapter}{\numberline {2}Setting up Source Control}{3}
\contentsline {section}{\numberline {2.1}Installing Git}{3}
\contentsline {section}{\numberline {2.2}Make a GitLab account}{3}
\contentsline {section}{\numberline {2.3}Fork a project in GitLab}{4}
\contentsline {section}{\numberline {2.4}Sharing the project with the instructor}{6}
\contentsline {section}{\numberline {2.5}Setting the project to private}{7}
\contentsline {section}{\numberline {2.6}Cloning the repository on your computer}{7}
\contentsline {paragraph}{Windows users:}{8}
\contentsline {paragraph}{Linux (and Mac) users:}{8}
\contentsline {paragraph}{Clone command:}{8}
\contentsline {section}{\numberline {2.7}Setting up the config}{9}
\contentsline {section}{\numberline {2.8}Add, Commit, and Push files}{10}
\contentsline {subsection}{\numberline {2.8.1}Update README.md}{10}
\contentsline {subsection}{\numberline {2.8.2}Add lab files}{11}
\contentsline {subsection}{\numberline {2.8.3}Add, Commit, and Push via Git}{11}
\contentsline {paragraph}{git status:}{12}
\contentsline {paragraph}{git add:}{12}
\contentsline {paragraph}{git status (again):}{13}
\contentsline {paragraph}{git commit:}{13}
\contentsline {paragraph}{git push:}{14}
\contentsline {section}{\numberline {2.9}Viewing the commit log and file history}{15}
\contentsline {subsection}{\numberline {2.9.1}The commit log}{15}
\contentsline {subsection}{\numberline {2.9.2}File history}{17}
\contentsline {paragraph}{Edit from the web:}{17}
\contentsline {paragraph}{Blame:}{17}
\contentsline {paragraph}{History:}{18}
\contentsline {section}{\numberline {2.10}Pulling latest changes}{19}
