\documentclass[a4paper,12pt,oneside]{book}
\usepackage[utf8]{inputenc}

\newcommand{\laTopic}       {Git and Source Control lab}
\newcommand{\laTitle}       {CS 235/250}

\renewcommand{\chaptername}{Part}
\renewcommand{\contentsname}{Contents - \laTopic}

\usepackage{../../rachwidgets}
\usepackage{../../rachdiagrams}

\title{}
\author{Rachel Singh}
\date{\today}

\pagestyle{fancy}
\fancyhf{}

\lhead{\laTopic \ / \laTitle}

\chead{}

\rhead{\thepage}

\rfoot{\tiny \thepage\ of \pageref{LastPage}}

\lfoot{\tiny Rachel Singh, last updated \today}

\renewcommand{\headrulewidth}{2pt}
\renewcommand{\footrulewidth}{1pt}

\begin{document}

	\tableofcontents

\chapter{Turn-in instructions}

	\begin{itemize}
		\item	Once done, copy-paste the URL to your repository webpage in the Canvas assignment.
		\item	Make sure your repository is set to private, but the instructor is set as a Guest or Maintainer
				(steps below during the lab).
	\end{itemize}

\chapter{Setting up Source Control}

	You should be tracking your work during this class in an online repository
	so that you can take advantange of source control tools. Make sure
	you read the instructor's notes on Git for additional reference information
	and background information on using this tool.
	
	You can also watch the video for this lab to help get through the steps.
	
	\section{Installing Git}
		
		First, you will need to install the Git software on your computer.
		You may be able to get Git through your operating system's package/software manager,
		or you can download it for free off the official website:
		
		\begin{center}
			https://git-scm.com/
		\end{center}
		
		Downloading and installing Git will usually set up two versions:
		Git GUI and Git Bash. We are going to use Git Bash because there are
		only about 7 commands to remember (or have on a reference sheet).
		
	\section{Make a GitLab account}
	
		Secondly, go to GitLab's website and create an account.
		
		\begin{center}
			https://www.gitlab.com/
		\end{center}
		
		GitLab is a source hosting service, similar to GitHub, BitBucket,
		and SourceForge. You can host as many private repositories as you'd like
		for free on GitLab.
		
	\section{Fork a project in GitLab}
	
		Once you've made your account, there will be a blank dashboard:
		
		\begin{center}
			\includegraphics[width=14cm]{images/gitlab-frontpage.png}
		\end{center}
		
		but you're going to fork an existing repository that the instructor
		already made. When you \textbf{fork} a repository, you basically
		make your own copy of it on your account.
		
		Go to the course's repository webpage, one of these:
	
		\begin{center}
			https://gitlab.com/rsingh13/cs235 ~\\~\\
			https://gitlab.com/rsingh13/cs250
		\end{center}
		
		On the upper-right side of the page will be a \textbf{Fork} button.
		Click on it.
		
		\begin{center}
			\includegraphics[width=14cm]{images/gitlab-fork.png}
		\end{center}
		
		Select your account to fork the repository to.
		
		\begin{center}
			\includegraphics[width=14cm]{images/gitlab-fork2.png}
		\end{center}
		
		You will now have a copy of the repository under your own account
		with a URL like this:
		
		\begin{center}
			https://gitlab.com/YOURNAME/cs235
		\end{center}
		
		When you go to your main profile page (https://gitlab.com/YOURNAME/), it will be listed under
		your \textbf{Personal projects}:
		
		\begin{center}
			\includegraphics[width=14cm]{images/gitlab-profile.png}
		\end{center}
		
	\newpage
	\section{Sharing the project with the instructor}
	
		Now you will have a blank project dashboard. There are a series
		of features on the left side of this page.

		\begin{center}
			\includegraphics[width=14cm]{images/gitlab-repo-frontpage.png}
		\end{center}
		
		Go to \textbf{Members} and under the header \textbf{GitLab member or Email address},
		put in my secondary GitLab account: \texttt{rsingh13} or \texttt{rsingh13@jccc.edu}
		(Rachel Singh).
		
		\begin{center}
			\includegraphics[width=14cm]{images/gitlab-invite.png}
		\end{center}
		
		The default permission is Guest, but you could also set me as a
		Maintainer in case you have difficulties with your repository.
		
	\newpage
	\section{Setting the project to private}
	
		On the left navbar in GitLab, click on the Settings icon.
		\includegraphics[height=1cm]{images/gitlab-settings.png}
		
		The second section under the settings is \textbf{Visibility, project features, permissions}.
		Open this up, and change \textbf{Project visibility} to \textbf{Private},
		then scroll to the bottom of the region and click the \textbf{Save changes} button.
		
	\section{Cloning the repository on your computer}
	
		Now we will pull down the repository to your machine. You can make
		changes to the folders and files locally on your machine, and then
		push the changes back up to the server to keep your code backed up.
		
		From the front page of your project repository, click on the
		blue \textbf{Clone} button. There will be two URLs. \textbf{Copy} the
		\textbf{HTTPS} version (unless you know how to set up an SSH key).
		
		\begin{center}
			\includegraphics[width=14cm]{images/gitlab-url.png}
		\end{center}
		
		\begin{hint}{Copy the correct URL!}
			Make sure you're copying the URL from the \textbf{Clone} button,
			NOT the browser URL! This is different!
		\end{hint}
		
		\paragraph{Windows users:}		
		On your system, navigate to a directory where you want to store
		your class assignments, such as \texttt{C:\textbackslash School\textbackslash}
		(for Windows users) and right-click an empty spot in the file explorer.
		\textbf{Open GitBash} should be an option now in the right-click menu.
		Click on this to open GitBash at this location.
		
		\paragraph{Linux (and Mac) users:}
		I'm not familiar with using Mac systems but I know you can use
		the terminal on it. With either of these systems, you will want
		to navigate to a directory for assignments (e.g. \texttt{/home/yourname/school/})
		and open the terminal there.
		
		\paragraph{Clone command:} Once the Bash/terminal is open, you can
		clone the repository to your hard-drive with this command:
		\texttt{git clone URL} , replacing ``URL'' with the URL you
		copied from the project page. Hit enter and it will clone to your computer.

		\begin{center}
			\includegraphics[width=14cm]{images/git-clone.png}
		\end{center}
		
		There will now be a folder you can open up on your computer at
		the path where you ran the clone command from.
		
		\begin{center}
			\includegraphics[width=14cm]{images/harddrive-repofolder.png}
		\end{center}
	
	\newpage
	\section{Setting up the config}
	
		Before you do any commits, Git will want you to set your username and email.
		Use these commands:
		
\begin{lstlisting}[style=output]
git config --global user.name "Your Name"
git config --global user.email "you@yourdomain.com"
\end{lstlisting}

	\newpage
	\section{Add, Commit, and Push files}
	
		Within the project file, there will already be some folders
		and files. For example:
		
		\begin{center}
			\includegraphics[width=14cm]{images/folder-2folders.png}
		\end{center}
		
		(The items in the latest version may be different from here.)
		
		\subsection{Update README.md}
		
			First, open your README.md file in a text editor like Notepad. A
			``.md'' file is a \textit{markdown} file - it is plaintext and
			uses special commands to indicate headers, links, etc. - you don't
			really have to worry about it.
			
			In the README file, just add some text, such as your name and the semester.
			
			\begin{center}
				\includegraphics[width=10cm]{images/folder-readmeb.png}
			\end{center}
		
			Save the file and close the editor.
			
		\newpage
		\subsection{Add lab files}
		
			You should have already gone through the \textbf{tools setup lab},
			so copy your tools setup lab folder (which should contain project files
			and the source code file) to within the lab folder in this repository.
		
			\begin{center}
				\includegraphics[width=14cm]{images/folder-addlab.png} ~\\
				\footnotesize The ``labs'' folder contains the ``00 Tools Setup Lab'' folder, \\
				which contains project and source files.
			\end{center}
			
		\subsection{Add, Commit, and Push via Git}
		
			Now navigate back to the base folder of your project. Right-click
			in the empty area and click on \textbf{Open Git Bash} or
			\textbf{Open Terminal} here.
		
			\begin{center}
				\includegraphics[width=10cm]{images/folder-openterminal.png}
			\end{center}
		
			\newpage
			\paragraph{git status:} The \texttt{git status} command shows you
			what has changed in your repository since the last time you committed anything.
			
			~\\ Try typing \texttt{git status} and hit enter:
			
\begin{lstlisting}[style=output]
git status
\end{lstlisting}

			\begin{center}
				\includegraphics[width=14cm]{images/git-status1.png}
			\end{center}
			
			\hrulefill

			\paragraph{git add:} The git add command allows you to add
			files that you want to keep track of to a new ``snapshot''.
			You can backup files at different points in time to keep track
			of the changes you've made to them.
			
			~\\ Use the following to add all changed files, except what is listed in the ``.gitignore''
			file (which I've already set up for you as part of the repository):
			
\begin{lstlisting}[style=output]
git add .
\end{lstlisting}

			Make sure you put a space between ``add'' and ``.''
			You might not see any feedback after entering this command.
			
			\newpage
			\paragraph{git status (again):} 
			Next, view the status again in the repository by using
			\texttt{git status}.
			
\begin{lstlisting}[style=output]
git status
\end{lstlisting}
			
			\begin{center}
				\includegraphics[width=14cm]{images/git-status2.png}
			\end{center}
			
			You can now see the changes that are going to be saved. This
			includes the change you made to the README.md file, as well
			as the new files you added.

			\hrulefill

			\paragraph{git commit:}
			Yse the following to make a snapshot of the state
			of the repository - commit all the pending changes.
			
\begin{lstlisting}[style=output]
git commit -m "Added tools lab"
\end{lstlisting}
		
			\begin{center}
				\includegraphics[width=14cm]{images/git-commit.png}
			\end{center}
			
			The ``-m'' flag lets you write a commit message in the same
			line as your \texttt{git commit} command. If you didn't have
			this part, it would open up a text editor for you to write
			the commit message in.

			\paragraph{git push:} You can make multiple commits while working
			on a new feature or assignment, but once you're ready to store
			the files on GitLab's server, you will need to use the push command:
			
\begin{lstlisting}[style=output]
git push
\end{lstlisting}
			
			\begin{center}
				\includegraphics[width=14cm]{images/git-push.png}
			\end{center}
			
			Now when you go to the project page online you will see the changes.
	
	\newpage
	\section{Viewing the commit log and file history}
	
		\subsection{The commit log}
	
			Back on the project webpage on GitLab, click on the \textbf{Repository $\to$ Commits}
			button from the left-side navigation bar.
		
			\begin{center}
				\includegraphics[width=10cm]{images/gitlab-commitlog.png}
			\end{center}
			
			You will be able to see the history of all the commits made for
			this project, including the commit message.
				
			\begin{center}
				\includegraphics[width=14cm]{images/gitlab-commitlog2.png}
			\end{center}
			
			Click on the most recent commit title and it will got to a summary
			page. It will highlight removed lines from files in red, and
			added lines to files in green.
				
			\begin{center}
				\includegraphics[width=14cm]{images/gitlab-commitlog3.png}
			\end{center}
			
			
			\begin{hint}{Commit logs to help debug}
				Something I like to do when I have to find a bug in my program
				is to commit all my files \textit{before} I figure out the problem,
				with a commit message like ``looking for problem with xyz''.
				Then, after I find and fix the error, I'll make a second commit
				with a message ``fixed error''.
				
				~\\
				This way, if I ever encounter the same error again, I will
				be able to locate the commit log where I found the same thing,
				and checking the differences in the commit, I can figure out
				what the cause of the issue was again!
			\end{hint}
		
		\newpage
		\subsection{File history}
		
			Go back to the main page of your project webpage and click on
			the README.md file to open up and view it.
		
			\begin{center}
				\includegraphics[width=14cm]{images/gitlab-readmeview.png}
			\end{center}
			
			\paragraph{Edit from the web:}
			If you ever want to make a quick edit to a file from the web,
			you can do so by clicking the \textbf{Edit} or \textbf{Web IDE}
			buttons. Click on one of these, add one more line of text to your
			README.md file, then click \textbf{Commit changes}.
			
			\paragraph{Blame:}
			Next, click on the \textbf{Blame} button. This will show you
			the file, and the origin of each line in that file - which commit,
			and who changed it.
			
			\begin{center}
				\includegraphics[width=14cm]{images/gitblame.png}
			\end{center}
			
			The ``Initial commit'' will be the commit from my \texttt{rsingh13}
			GitLab account, but commits afterwards will be from your account.
			
			\paragraph{History:} Finally, click on the \textbf{History} button
			(it was next to the Blame button) and you'll see a list of all
			commits to this repository that affected this file.
			
			\begin{center}
				\includegraphics[width=14cm]{images/githistory.png}
			\end{center}
			
			You'll make commits during each programming assignment you work on,
			so not all commits will affect every file. If you want to \textit{only}
			see commits related to changes to a specific file, you can view the
			file's history like this.
			
	\newpage
	\section{Pulling latest changes}
	
		Since we made a change to the README.md file online, we will need to
		pull the latest changes from the server back to our local harddrive.
		This is also something that you will need to do if you're working on a project with another person,
		or if you've copied your repository to different computers to work
		from different places. ~\\
		
		Back in your project terminal, pull all the most recent changes with
		
\begin{lstlisting}[style=output]
git pull
\end{lstlisting}

		\begin{center}
			\includegraphics[width=14cm]{images/git-pull.png}
		\end{center}
		
		\begin{hint}{Pull then Push}
		If, for some reason, git isn't letting you \textbf{push} to the server,
		chances are is that there are files that need to be pulled down and updated
		\textit{before} you can push your changes - so do a pull before a push
		if you're working on multiple machines or with somebody else.
		\end{hint}
		
		\vspace{1cm}
		
		\begin{center}
			After you've done these changes, post the URL to the project webpage
			(NOT the git URL!) to the Canvas assignment to receive credit for this assignment.
			Note that your repository should be \textbf{private}, but have the 
			instructor set as a \textbf{Guest} or \textbf{Maintainer}.
		\end{center}
			
\end{document}

