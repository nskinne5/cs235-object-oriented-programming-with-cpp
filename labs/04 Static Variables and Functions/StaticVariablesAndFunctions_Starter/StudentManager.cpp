#include "StudentManager.hpp"

#include <stdexcept>    // runtime_error
#include <iomanip>      // setw
#include <iostream>     // cin/cout
using namespace std;

// Required for static variable:
vector<Student> StudentManager::students;

void StudentManager::AddStudent( string studentName, int studentId )
{
    throw runtime_error( "Function not implemented!" );
}

void StudentManager::RemoveStudent( int studentId )
{
    throw runtime_error( "Function not implemented!" );
}

Student& StudentManager::GetStudent( int studentId )
{
    throw runtime_error( "Function not implemented!" );
}

void StudentManager::ListStudents()
{
    throw runtime_error( "Function not implemented!" );
}

int StudentManager::GetStudentCount()
{
    throw runtime_error( "Function not implemented!" );
}

int StudentManager::GetIndexOf( int studentId )
{
    throw runtime_error( "Function not implemented!" );
}

int StudentManager::GetIndexOf( string studentName )
{
    throw runtime_error( "Function not implemented!" );
}
