#include <iostream>
using namespace std;

#include "File.hpp"

int main()
{
    File fileA;
    fileA.Create( "myfile", "txt" );
    fileA.AddContents( "We can dance if we want to." );
    fileA.AddContents( "We can leave your friends behind." );
    fileA.AddContents( "'cuz your friends don't dance," );
    fileA.AddContents( "and if they don't dance," );
    fileA.AddContents( "well they're no friends of mine." );

    cout << endl << "FILE A:" << endl;

    fileA.DisplayInfo();
    fileA.DisplayContents();

    cout << endl << "FILE B:" << endl;
    // Create a fileB, copy the contents from fileA.


    return 0;
}
